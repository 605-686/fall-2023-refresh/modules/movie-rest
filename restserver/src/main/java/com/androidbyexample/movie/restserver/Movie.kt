package com.androidbyexample.movie.restserver

import com.fasterxml.jackson.annotation.JsonProperty
import java.util.UUID
import javax.xml.bind.annotation.XmlRootElement

@XmlRootElement
data class Movie(
    @JsonProperty("id") val id: String = UUID.randomUUID().toString(),
    @JsonProperty("title") val title: String,
    @JsonProperty("description") val description: String,
    @JsonProperty("ratingId") val ratingId: String,
)

@XmlRootElement
data class MovieWithCast(
    @JsonProperty("movie") val movie: Movie,
    @JsonProperty("cast") val cast: List<RoleWithActor>,
)

@XmlRootElement
data class RoleWithActor(
    @JsonProperty("actor") val actor: Actor,
    @JsonProperty("character") val character: String,
    @JsonProperty("orderInCredits") val orderInCredits: Int,
)
