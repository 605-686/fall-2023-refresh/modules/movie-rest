package com.androidbyexample.movie.screens

import androidx.activity.compose.BackHandler
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.rememberCoroutineScope
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.androidbyexample.movie.MovieViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

@Composable
fun Ui(
    viewModel: MovieViewModel,
    onExit: () -> Unit,
) {
    BackHandler {
        viewModel.popScreen()
    }

    val scope = rememberCoroutineScope()

    when (val screen = viewModel.currentScreen) {
        null -> onExit()
        is MovieDisplay -> {
            val selectedIds by viewModel.selectedIdsFlow.collectAsStateWithLifecycle(initialValue = emptySet())
            MovieDisplayUi(
                id = screen.id,
                fetchMovie = viewModel::getMovieWithCast,
                onActorClicked = { viewModel.pushScreen(ActorDisplay(it.actor.id)) },

                selectedIds = selectedIds,
                onClearSelections = viewModel::clearSelectedIds,
                onSelectionToggle = viewModel::toggleSelection,
                onDeleteSelectedMovies = viewModel::deleteSelectedMovies,
                currentScreen = screen,
                onSelectListScreen = viewModel::setScreen,
                onEdit = { viewModel.pushScreen(MovieEdit(it)) },
                onResetDatabase = {
                    scope.launch(Dispatchers.IO) {
                        viewModel.resetDatabase()
                    }
                }
            )
        }
        is MovieEdit -> {
            MovieEditUi(
                id = screen.id,
                fetchMovie = viewModel::getMovie,
                onMovieChange = viewModel::update
            )
        }
        is ActorDisplay -> {
            val selectedIds by viewModel.selectedIdsFlow.collectAsStateWithLifecycle(initialValue = emptySet())
            ActorDisplayUi(
                id = screen.id,
                fetchActor = viewModel::getActorWithFilmography,
                onMovieClicked = { viewModel.pushScreen(MovieDisplay(it.id)) },

                selectedIds = selectedIds,
                onClearSelections = viewModel::clearSelectedIds,
                onSelectionToggle = viewModel::toggleSelection,
                onDeleteSelectedMovies = viewModel::deleteSelectedMovies,
                currentScreen = screen,
                onSelectListScreen = viewModel::setScreen,
                onResetDatabase = {
                    scope.launch(Dispatchers.IO) {
                        viewModel.resetDatabase()
                    }
                }
            )
        }
        is RatingDisplay -> {
            val selectedIds by viewModel.selectedIdsFlow.collectAsStateWithLifecycle(initialValue = emptySet())
            val ratingWithMovies by viewModel.getRatingWithMoviesFlow(screen.id).collectAsStateWithLifecycle(initialValue = null)
            RatingDisplayUi(
                ratingWithMovies = ratingWithMovies,
                onMovieClicked = { viewModel.pushScreen(MovieDisplay(it.id)) },
                selectedIds = selectedIds,
                onClearSelections = viewModel::clearSelectedIds,
                onSelectionToggle = viewModel::toggleSelection,
                onDeleteSelectedMovies = viewModel::deleteSelectedMovies,
                currentScreen = screen,
                onSelectListScreen = viewModel::setScreen,
                onResetDatabase = {
                    scope.launch(Dispatchers.IO) {
                        viewModel.resetDatabase()
                    }
                }
            )
        }
        MovieList -> {
            val movies by viewModel.moviesFlow.collectAsStateWithLifecycle(initialValue = emptyList())
            val selectedIds by viewModel.selectedIdsFlow.collectAsStateWithLifecycle(initialValue = emptySet())
            MovieListUi(
                movies = movies,
                onMovieClicked = { movie ->
                    viewModel.pushScreen(MovieDisplay(movie.id))
                },
                selectedIds = selectedIds,
                onClearSelections = viewModel::clearSelectedIds,
                onSelectionToggle = viewModel::toggleSelection,
                onDeleteSelectedMovies = viewModel::deleteSelectedMovies,
                currentScreen = screen,
                onSelectListScreen = viewModel::setScreen,
                onResetDatabase = {
                    scope.launch(Dispatchers.IO) {
                        viewModel.resetDatabase()
                    }
                }
            )
        }
        ActorList -> {
            val actors by viewModel.actorsFlow.collectAsStateWithLifecycle(initialValue = emptyList())
            val selectedIds by viewModel.selectedIdsFlow.collectAsStateWithLifecycle(initialValue = emptySet())
            ActorListUi(
                actors = actors,
                onActorClicked = { actor ->
                    viewModel.pushScreen(ActorDisplay(actor.id))
                },
                selectedIds = selectedIds,
                onClearSelections = viewModel::clearSelectedIds,
                onSelectionToggle = viewModel::toggleSelection,
                onDeleteSelectedActors = viewModel::deleteSelectedActors,
                currentScreen = screen,
                onSelectListScreen = viewModel::setScreen,
                onResetDatabase = {
                    scope.launch(Dispatchers.IO) {
                        viewModel.resetDatabase()
                    }
                }
            )
        }
        RatingList -> {
            val ratings by viewModel.ratingsFlow.collectAsStateWithLifecycle(initialValue = emptyList())
            val selectedIds by viewModel.selectedIdsFlow.collectAsStateWithLifecycle(initialValue = emptySet())
            RatingListUi(
                ratings = ratings,
                onRatingClicked = { rating ->
                    viewModel.pushScreen(RatingDisplay(rating.id))
                },
                selectedIds = selectedIds,
                onClearSelections = viewModel::clearSelectedIds,
                onSelectionToggle = viewModel::toggleSelection,
                onDeleteSelectedRatings = viewModel::deleteSelectedRatings,
                currentScreen = screen,
                onSelectListScreen = viewModel::setScreen,
                onResetDatabase = {
                    scope.launch(Dispatchers.IO) {
                        viewModel.resetDatabase()
                    }
                }
            )
        }
    }
}
