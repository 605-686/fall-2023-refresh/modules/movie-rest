package com.androidbyexample.movie.data

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.Relation
import java.util.UUID

@Entity
data class ActorEntity(
    @PrimaryKey var id: String = UUID.randomUUID().toString(),
    var name: String,
)

// POKO that groups an Actor with the movies they appear in, and the
//   role played in each movie. This is a ONE-TO-MANY relation from the
//   ActorEntity to the RoleEntity, where we resolve the RoleEntity's movieId
//   to fetch the actual movie (a MANY-TO-ONE relation). The end result is turning
//   a MANY-TO-MANY relation into two ONE-TO-MANY relationships, where the MANY part
//   is the RoleEntity.

data class ActorWithFilmography(
    @Embedded
    val actor: ActorEntity,

    @Relation(
        entity = RoleEntity::class,
        parentColumn = "id",
        entityColumn = "actorId",
    )
    val rolesWithMovies: List<RoleWithMovie>,
)

data class RoleWithMovie(
    @Embedded
    val role: RoleEntity,

    @Relation(
        parentColumn = "movieId",
        entityColumn = "id"
    )
    val movie: MovieEntity,
)
