@Suppress("DSL_SCOPE_VIOLATION") // TODO: Remove once KTIJ-19369 is fixed
plugins {
    application
    alias(libs.plugins.org.jetbrains.kotlin.jvm)
}

kotlin {
    jvmToolchain(17)
}

dependencies {
    // example of using a version catalog bundle to pull in multiple deps
    implementation(libs.bundles.server)
}

application {
    mainClass.set("com.androidbyexample.movie.restserver.RunServerKt")
}