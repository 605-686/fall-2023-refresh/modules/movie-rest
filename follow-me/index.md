---
title: Movie REST
---

Android applications often communicate with servers to manage common data. In this module, we'll look at RESTful Web Services and see how your application can communicate with them to manage remote data.

## Videos

The videos from previous class sessions explain the concepts well, but the code is slightly different. I've updated the code to the new structure and based it off our current movie application.

### REST: Lecture (Fall 2021) (07:04)

<iframe width="800" height="450" src="https://www.youtube.com/embed/Fd0L5LbEwPU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### REST: Example (Fall 2021) (18:44)

<iframe width="800" height="450" src="https://www.youtube.com/embed/Ya7sCOzl0ss" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Source code for examples is at
[https://gitlab.com/605-686/fall-2023-refresh/modules/movie-rest](https://gitlab.com/605-686/fall-2023-refresh/modules/movie-rest)
