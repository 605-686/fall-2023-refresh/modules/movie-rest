package com.androidbyexample.movie.repository

import com.androidbyexample.movie.data.ActorEntity
import com.androidbyexample.movie.data.ActorWithFilmography
import com.androidbyexample.movie.data.RoleWithMovie

data class ActorDto(
    override val id: String,
    val name: String,
): HasId

internal fun ActorEntity.toDto() =
    ActorDto(id = id, name = name)
internal fun ActorDto.toEntity() =
    ActorEntity(id = id, name = name)

data class ActorWithFilmographyDto(
    val actor: ActorDto,
    val filmography: List<RoleWithMovieDto>,
)

data class RoleWithMovieDto(
    val movie: MovieDto,
    val character: String,
    val orderInCredits: Int,
): HasId {
    override val id: String
        get() = "${movie.id}:$character"
}

internal fun RoleWithMovie.toDto() =
    RoleWithMovieDto(
        movie = movie.toDto(),
        character = role.character,
        orderInCredits = role.orderInCredits,
    )

internal fun ActorWithFilmography.toDto() =
    ActorWithFilmographyDto(
        actor = actor.toDto(),
        filmography =
        rolesWithMovies.map {
            it.toDto()
        }
    )