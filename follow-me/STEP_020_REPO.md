---
title: Changes for REST
template: main-repo.html
---

Be sure to watch the videos on the previous page before looking at these code changes.

Here you can see the changes made from our movie example so far to include the REST support.
I've added comments to some sections to explain things that might not be obvious.

A few notes:

## RestServer module

I added a `restserver` module. Normally this wouldn't be included with your application code,
but it's here to keep things together and simple. This module uses Jersey to create a simple
REST server.

## MovieRestRepository

I added a `MovieRestRepository` that uses `MovieApiService` (using Retrofit as a REST client).
Note that we need a coroutine scope here. We want to use the viewModelScope so the coroutines
stay alive as long as the view model, but we end up with a chicken-and-egg problem:
  
* We can't create the view model without a repository
* We can't create the repository without the viewModelScope
  
There are a few ways to handle this. One approach would be to allow the view model to set the
coroutine scope in the passed-in repository. That would keep things pretty much the same as
they are with delegation and passing-in of the repository.

An alternate approach is to pass in a factory to create the view model instance. I chose to
demonstrate this approach as it ensures the repository must have a scope,
and has the side effect that you must delegate from the view model to the repository explicitly
(rather than using `MovieRepository by repository`) as we did before.

See the comments in the `MovieViewModel` for more details.

## All code changes
