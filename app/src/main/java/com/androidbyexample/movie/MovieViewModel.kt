package com.androidbyexample.movie

import android.app.Application
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.glance.appwidget.updateAll
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.CreationExtras
import com.androidbyexample.movie.glance.MovieAppWidget
import com.androidbyexample.movie.repository.ActorDto
import com.androidbyexample.movie.repository.MovieDto
import com.androidbyexample.movie.repository.MovieRepository
import com.androidbyexample.movie.repository.MovieRestRepository
import com.androidbyexample.movie.repository.RatingDto
import com.androidbyexample.movie.screens.MovieList
import com.androidbyexample.movie.screens.Screen
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.launch

@OptIn(FlowPreview::class)
class MovieViewModel(
    application: Application,
    // The rest repository needs a coroutine scope
    // We want to use the viewModelScope, but that's not
    //   available until the view model has been created
    // The view model couldn't be created without a repository
    // To fix this, instead of passing a repository instance,
    //   we pass a factory function to create one, so we can
    //   pass our viewModelScope into it
    repositoryFactory: (CoroutineScope) -> MovieRepository
): AndroidViewModel(application) {
    // Here we create the repository instance by passing the viewModelScope
    // This creates a problem - because we cannot pass the repository
    //   as a parameter to the view model, we cannot implement the
    //   MovieRepository interface and delegate to it using "by"
    // This means we need to explicitly define any functions we
    //   need to delegate to the repository.
    private val repository = repositoryFactory(viewModelScope)
    private val _selectedIdsFlow = MutableStateFlow<Set<String>>(emptySet())
    val selectedIdsFlow: Flow<Set<String>> = _selectedIdsFlow.asStateFlow()

    fun clearSelectedIds() {
        _selectedIdsFlow.value = emptySet()
    }
    fun toggleSelection(id: String) {
        if (id in _selectedIdsFlow.value) {
            _selectedIdsFlow.value -= id
        } else {
            _selectedIdsFlow.value += id
        }
    }

    private var screenStack = listOf<Screen>(MovieList)
        set(value) {
            field = value
            clearSelectedIds()
            currentScreen = value.lastOrNull()
        }

    // NOTE: We're keep this as a Compose State for comparison.
    //       You can use Compose state to expose anything from the view model,
    //       but our example will be using Flow from now on to demonstrate how
    //       the view model can be used without Compose, perhaps for other
    //       platforms such as iOS, desktop, web or command line
    var currentScreen by mutableStateOf<Screen?>(MovieList)
        private set

    fun pushScreen(screen: Screen) {
        screenStack = screenStack + screen
    }
    fun popScreen() {
        screenStack = screenStack.dropLast(1)
    }

    fun setScreen(screen: Screen) {
        screenStack = listOf(screen)
    }

    fun setScreens(vararg screens: Screen) {
        screenStack = screens.toList()
    }

    // explicitly delegate the flows to the repository
    val moviesFlow = repository.moviesFlow
    val actorsFlow = repository.actorsFlow
    val ratingsFlow = repository.ratingsFlow

    fun update(movie: MovieDto) {
        movieUpdateFlow.value = movie
    }
    fun update(actor: ActorDto) {
        actorUpdateFlow.value = actor
    }
    fun update(rating: RatingDto) {
        ratingUpdateFlow.value = rating
    }

    // using a debounced flow as a person-update queue
    private val movieUpdateFlow = MutableStateFlow<MovieDto?>(null)
    private val actorUpdateFlow = MutableStateFlow<ActorDto?>(null)
    private val ratingUpdateFlow = MutableStateFlow<RatingDto?>(null)
    init {
        viewModelScope.launch {
            repository.moviesFlow.collectLatest {
                MovieAppWidget().updateAll(getApplication())
            }
        }
        viewModelScope.launch {
            movieUpdateFlow.debounce(500).collect { movie ->
                movie?.let { repository.upsert(it) }
            }
        }
        viewModelScope.launch {
            actorUpdateFlow.debounce(500).collect { actor ->
                actor?.let { repository.upsert(it) }
            }
        }
        viewModelScope.launch {
            ratingUpdateFlow.debounce(500).collect { rating ->
                rating?.let { repository.upsert(it) }
            }
        }
    }

    // explicitly delegate these functions to the repository
    suspend fun getMovieWithCast(id: String) = repository.getMovieWithCast(id)
    suspend fun getMovie(id: String) = repository.getMovie(id)
    suspend fun getActorWithFilmography(id: String) = repository.getActorWithFilmography(id)
    fun getRatingWithMoviesFlow(id: String) = repository.getRatingWithMoviesFlow(id)

    suspend fun deleteActorsById(ids: Set<String>) {
        repository.deleteActorsById(ids)
    }
    suspend fun deleteMoviesById(ids: Set<String>) {
        repository.deleteMoviesById(ids)
    }
    suspend fun deleteRatingsById(ids: Set<String>) {
        repository.deleteRatingsById(ids)
    }
    suspend fun resetDatabase() {
        repository.resetDatabase()
    }

    fun deleteSelectedMovies() {
        viewModelScope.launch {
            deleteMoviesById(_selectedIdsFlow.value)
            _selectedIdsFlow.value = emptySet()
        }
    }
    fun deleteSelectedActors() {
        viewModelScope.launch {
            deleteActorsById(_selectedIdsFlow.value)
            _selectedIdsFlow.value = emptySet()
        }
    }
    fun deleteSelectedRatings() {
        viewModelScope.launch {
            deleteRatingsById(_selectedIdsFlow.value)
            _selectedIdsFlow.value = emptySet()
        }
    }

    companion object {
        val Factory: ViewModelProvider.Factory = object: ViewModelProvider.Factory {
            @Suppress("UNCHECKED_CAST")
            override fun <T : ViewModel> create(
                modelClass: Class<T>,
                extras: CreationExtras
            ): T {
                // Get the Application object from extras
                val application = checkNotNull(extras[ViewModelProvider.AndroidViewModelFactory.APPLICATION_KEY])
                return MovieViewModel(application) { scope ->
                    MovieRestRepository(scope)
                } as T
            }
        }
    }
}
