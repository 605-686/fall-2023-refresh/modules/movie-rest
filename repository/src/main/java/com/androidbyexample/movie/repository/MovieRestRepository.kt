package com.androidbyexample.movie.repository

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Response

class MovieRestRepository(
    val coroutineScope: CoroutineScope,
): MovieRepository {
    // I abstracted this a bit more from the ListFlowManager in the video
    // It now works for lists and single items
    inner class FlowManager<T>(
        private val defaultValue: T,
        private val fetcher: suspend () -> Response<T>
    ) {
        private val _flow = MutableStateFlow(defaultValue)
        init {
            fetch()
        }
        val flow: Flow<T>
            get() = _flow

        fun fetch() =
            coroutineScope.launch(Dispatchers.IO) {
                _flow.value = fetcher().takeIf { it.isSuccessful }?.body() ?: defaultValue
            }
    }

    private val movieApiService = MovieApiService.create()

    private val moviesFlowManager = FlowManager(emptyList()) { movieApiService.getMovies() }
    private val actorsFlowManager = FlowManager(emptyList()) { movieApiService.getActors() }
    private val ratingsFlowManager = FlowManager(emptyList()) { movieApiService.getRatings() }

    override val ratingsFlow: Flow<List<RatingDto>> = ratingsFlowManager.flow
    override val moviesFlow: Flow<List<MovieDto>> = moviesFlowManager.flow
    override val actorsFlow: Flow<List<ActorDto>> = actorsFlowManager.flow

    // NOTE: The following assume that only one of each is active at a time
    //       For our app, that should be the case (the edit screens don't allow
    //       any deeper navigation), but if you wanted to be more general, you
    //       could set up a WeakHashMap where the key is the flow and the value
    //       is the manager (which also has a weak reference to the Flow).
    //       Weak references allow the object they point to to be garbage collected
    //       when there are no remaining strong references to it, and they'll be removed
    //       from the map when no longer referenced. You would then walk through all the
    //       values in the map and call their fetch() functions.

    private var ratingWithMoviesFlowManager: FlowManager<RatingWithMoviesDto?>? = null
    // not implementing the ActorWithFilmographyDto and MovieWithCastDto versions as we
    //   don't use them in the app right now
    override fun getRatingWithMoviesFlow(id: String) =
        FlowManager(null) {
            movieApiService.getRatingWithMovies(id)
        }.let {
            ratingWithMoviesFlowManager = it
            it.flow
        }


    private suspend fun <T> getOrError(
        id: String,
        fetch: suspend MovieApiService.(String) -> Response<T?>
    ): T = withContext(Dispatchers.IO) {
        movieApiService.fetch(id).takeIf {
            val code = it.code()
            it.isSuccessful
        }?.body()
            ?: throw RuntimeException("$id not found")
    }

    override suspend fun getRatingWithMovies(id: String) =
        getOrError(id) { getRatingWithMovies(it) }


    override suspend fun getMovieWithCast(id: String) =
        getOrError(id) { getMovieWithCast(it) }

    override suspend fun getActorWithFilmography(id: String) =
        getOrError(id) { getActorWithFilmography(it) }

    override suspend fun getMovie(id: String) =
        getOrError(id) { getMovie(it) }

    override suspend fun insert(movie: MovieDto) {
        movieApiService.createMovie(movie)
        moviesFlowManager.fetch()
        ratingWithMoviesFlowManager?.fetch()
    }

    override suspend fun insert(actor: ActorDto) {
        movieApiService.createActor(actor)
        actorsFlowManager.fetch()
    }

    override suspend fun insert(rating: RatingDto) {
        movieApiService.createRating(rating)
        ratingsFlowManager.fetch()
    }

    override suspend fun upsert(movie: MovieDto) {
        movieApiService.updateMovie(movie.id, movie)
        moviesFlowManager.fetch()
        ratingWithMoviesFlowManager?.fetch()
    }

    override suspend fun upsert(actor: ActorDto) {
        movieApiService.updateActor(actor.id, actor)
        actorsFlowManager.fetch()
    }

    override suspend fun upsert(rating: RatingDto) {
        movieApiService.updateRating(rating.id, rating)
        ratingsFlowManager.fetch()
    }

    private suspend fun deleteById(
        ids: Set<String>,
        delete: suspend MovieApiService.(String) -> Unit,
        vararg flowManagersToFetch: FlowManager<*>?,
    ) {
        ids.forEach { id ->
            movieApiService.delete(id)
        }
        flowManagersToFetch.forEach { it?.fetch() }
    }
    override suspend fun deleteMoviesById(ids: Set<String>) {
        deleteById(ids, { deleteMovie(it) }, moviesFlowManager, ratingWithMoviesFlowManager)
    }
    override suspend fun deleteActorsById(ids: Set<String>) {
        deleteById(ids, { deleteActor(it) }, actorsFlowManager)
    }
    override suspend fun deleteRatingsById(ids: Set<String>) {
        deleteById(ids, { deleteRating(it) }, ratingsFlowManager, moviesFlowManager)
    }


    override suspend fun resetDatabase() {
        movieApiService.reset()
        actorsFlowManager.fetch()
        moviesFlowManager.fetch()
        ratingsFlowManager.fetch()
    }
}