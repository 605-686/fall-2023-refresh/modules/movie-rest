package com.androidbyexample.movie.repository

import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.PUT
import retrofit2.http.Path

const val BASE_URL = "http://10.0.2.2:8080" // host computer for emulator
// NOTE: In a real app, you might allow the user to set this via settings

interface MovieApiService {
    @GET("movie")
    suspend fun getMovies(): Response<List<MovieDto>>

    @GET("actor")
    suspend fun getActors(): Response<List<ActorDto>>

    @GET("rating")
    suspend fun getRatings(): Response<List<RatingDto>>

    @GET("rating/{id}/movies")
    suspend fun getRatingWithMovies(@Path("id") id: String): Response<RatingWithMoviesDto?>
    @GET("movie/{id}/cast")
    suspend fun getMovieWithCast(@Path("id") id: String): Response<MovieWithCastDto?>
    @GET("actor/{id}/filmography")
    suspend fun getActorWithFilmography(@Path("id") id: String): Response<ActorWithFilmographyDto?>
    @GET("movie/{id}")
    suspend fun getMovie(@Path("id") id: String): Response<MovieDto?>

    @POST("movie")
    suspend fun createMovie(@Body movie: MovieDto): Response<MovieDto>

    @POST("actor")
    suspend fun createActor(@Body actor: ActorDto): Response<ActorDto>

    @POST("rating")
    suspend fun createRating(@Body rating: RatingDto): Response<RatingDto>

    @PUT("movie/{id}")
    suspend fun updateMovie(
        @Path("id") id: String,
        @Body movie: MovieDto
    ): Response<Int> // number updated

    @PUT("actor/{id}")
    suspend fun updateActor(
        @Path("id") id: String,
        @Body actor: ActorDto
    ): Response<Int> // number updated

    @PUT("rating/{id}")
    suspend fun updateRating(
        @Path("id") id: String,
        @Body rating: RatingDto
    ): Response<Int> // number updated

    @DELETE("rating/{id}")
    suspend fun deleteRating(
        @Path("id") id: String,
    ): Response<Int> // number deleted
    @DELETE("movie/{id}")
    suspend fun deleteMovie(
        @Path("id") id: String,
    ): Response<Int> // number deleted
    @DELETE("actor/{id}")
    suspend fun deleteActor(
        @Path("id") id: String,
    ): Response<Int> // number deleted

    @GET("reset")
    suspend fun reset(): Response<Int>

    companion object {
        fun create() =
            Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL)
                .build()
                .create(MovieApiService::class.java)
    }
}