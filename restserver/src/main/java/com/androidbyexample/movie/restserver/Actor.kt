package com.androidbyexample.movie.restserver

import com.fasterxml.jackson.annotation.JsonProperty
import java.util.UUID
import javax.xml.bind.annotation.XmlRootElement

@XmlRootElement
data class Actor(
    @JsonProperty("id") var id: String = UUID.randomUUID().toString(),
    @JsonProperty("name") var name: String
)

@XmlRootElement
data class ActorWithFilmography(
    @JsonProperty("actor") val actor: Actor,
    @JsonProperty("filmography") val filmography: List<RoleWithMovie>,
)

@XmlRootElement
data class RoleWithMovie(
    @JsonProperty("movie") val movie: Movie,
    @JsonProperty("character") val character: String,
    @JsonProperty("orderInCredits") val orderInCredits: Int,
)

