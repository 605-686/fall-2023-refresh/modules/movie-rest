package com.androidbyexample.movie.data

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index

// RoleEntity - note that it's considered a child of both MovieEntity and
//    ActorEntity. When either parent is deleted, we delete this entity. If
//    either parent's key is updated, we update this entity.
@Entity(
    primaryKeys = ["actorId", "movieId"],
    indices = [
        Index("movieId"),
        Index("actorId")
    ],
    foreignKeys = [
        ForeignKey(
            entity = MovieEntity::class,
            parentColumns = ["id"],
            childColumns = ["movieId"],
            onUpdate = ForeignKey.CASCADE,
            onDelete = ForeignKey.CASCADE,
        ),
        ForeignKey(
            entity = ActorEntity::class,
            parentColumns = ["id"],
            childColumns = ["actorId"],
            onUpdate = ForeignKey.CASCADE,
            onDelete = ForeignKey.CASCADE,
        )
    ]
)
data class RoleEntity(
    var movieId: String,
    var actorId: String,
    var character: String,
    var orderInCredits: Int,
)
